﻿/**
C identifiers & types to D conversion functions.

Copyright: Denis Shelomovskij 2014

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module bindgl.targets.d.ctod;


import std.algorithm;
import std.array;
import std.exception;
import std.string;

import bindgl.utils.ctypeparser;


@safe pure:

// C identifier to D
// ----------------------------------------------------------------------------------------------------

string toDID(string cID) nothrow
{
	return dNewKeywords.canFind(cID) ? cID ~ '_' : cID;
}

immutable string[] dNewKeywords = q{
abstract alias align asm assert
body bool byte
cast catch cdouble cent cfloat class creal
dchar debug delegate delete deprecated
export
false final finally foreach foreach_reverse function
idouble ifloat immutable import in inout interface invariant ireal is
lazy
macro mixin module
new nothrow null
out override
package pragma private protected public pure
real ref
scope shared super synchronized
template this throw true try typeid typeof
ubyte ucent uint ulong unittest ushort
version
wchar with
}.split();

nothrow unittest
{
	assert("a".toDID() == "a");
	assert("is".toDID() == "is_");
}

// C type to D
// ----------------------------------------------------------------------------------------------------

string toDType(in CType cType) @trusted
{
	const consts = (cType.const_ ~ cType.indirections.dup).group().array();
	enforce(consts.length <= 1 + cType.const_,
		format("Unsupported C type '%s'. D doesn't have head const.", cType));

	string name = cType.name;
	if(const res = cType.name in cToDTypes)
		name = *res;
	return (cType.const_ ? consts.length == 1 ? "const " : "const(" : "") ~
		name ~
		"".rightJustify(consts[0][1] - 1, '*') ~
		(consts.length == 1 ? "" : ")".leftJustify(consts[1][1] + 1, '*'));
}

immutable string[string] cToDTypes;

nothrow shared static this()
{
	cToDTypes =
	[
		`signed char`         :  `byte`    ,
		`unsigned char`       :  `ubyte`   ,
		`unsigned short`      :  `ushort`  ,
		`unsigned`            :  `uint`    ,
		`long`                :  `c_long`  ,
		`unsigned long`       :  `c_ulong` ,
		`long long`           :  `long`    ,
		`unsigned long long`  :  `ulong`   ,
		`long double`         :  `real`    ,
	];
};

unittest
{
	void test(in string cType, in string dType = null)
	{
		assert((const CType(cType)).toDType() == (dType ? dType : cType));
	}

	test("a");
	test("int");
	test("long", "c_long");
	test("double long", "real");

	test("const int");
	test("const int*", "const(int)*");
	test("int const*", "const(int)*");
	test("const int*const", "const int*");
	test("const int*const*", "const(int*)*");
	test("const int*const*const", "const int**");
	test("int*const").assertThrown();
	test("const int**const").assertThrown();
}
