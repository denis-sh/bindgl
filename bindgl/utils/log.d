﻿/**
Trivial log functions.

Copyright: Denis Shelomovskij 2014

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module bindgl.utils.log;


import std.stdio;


@safe:

enum Level { errors, warnings, info }

int level = Level.info;

void info(A...)(int type, string fmt, A args) @trusted
{
	if(level < Level.info + type)
		return;
	write("INFO: ");
	writefln(fmt, args);
}

void warning(A...)(string fmt, A args) @trusted
{
	if(level < Level.warnings)
		return;
	write("WARN: ");
	writefln(fmt, args);
}
