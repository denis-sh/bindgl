﻿/**
XML deserialization functions.

Copyright: Denis Shelomovskij 2014

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module bindgl.utils.xmldeserialization;


import std.array;
import std.conv;
import std.exception;
import std.string;
import std.traits;
import xml = std.xml;


@safe:

T deserialize(T)(string xmlStr) @trusted
{
	auto xml = new DocumentParser(xmlStr);
	enforce(xml.tag);
	enforce(xml.tag.name == xmlName!T);
	T res;
	parse(res, xml);
	return res;
}

private:

void parse(T)(ref T t, xml.ElementParser xml) @trusted
{
	foreach(i, ref field; t.tupleof)
	{
		alias F = typeof(field);
		enum isValue = isIntegral!F || isFloatingPoint!F || is(F == string);
		alias udas = expressionTuple!(__traits(getAttributes, t.tupleof[i]));
		enum id = __traits(identifier, t.tupleof[i]);
		enum errorText = F.stringof ~ ' ' ~ id;

		static if(isValue || is(F == string[]))
		{
			static if(udas.length)
			{
				static assert(udas.length == 1, errorText);

				enum string udaStr = udas[0];
				enum size_t tagLength = udaStr.indexOf(':');
				enum isElement = tagLength != -1;
				static assert(isElement || isValue, errorText);
				enum attrName = udaStr[tagLength + 1 .. $];
				static if(isElement)
					enum tagName = tagLength ? udaStr[0 .. tagLength] : id;
			}
			else
			{
				static assert(isValue || id == "texts", errorText);

				enum attrName = id;
			}

			static if(isValue)
			{
				static if(__traits(compiles, tagName))
				{
					static if(hasMember!(T, "texts"))
					{
						static assert(is(F == size_t), errorText);
						static assert(attrName.empty, errorText);
						field = -1;
					}

					xml.onEndTag[tagName] = (in Element e)
					{
						static if(!hasMember!(T, "texts"))
						{
							field = e.getTextOrAttrText(attrName).to!F();
						}
						else
						{
							assert(e.items.empty || e.items.length == 1 && cast(Text) e.items[0]);

							if(e.items.empty)
								t.texts ~= null;
							else
								enforce((cast(Text) e.items[0]).toString() == t.texts[$ - 1]);

							field = t.texts.length - 1;
						}
					};
				}
				else
				{
					field = xml.tag.tryGetAttrText(attrName).to!F();
				}
			}
			else static if(id != "texts") // F == string[]
			{
				xml.onEndTag[tagName] = (in Element e)
				{ field ~= e.getTextOrAttrText(attrName); };
			}
			else // string[] texts
			{
				xml.onText = (string s)
				{ field ~= s; };
			}
		}
		else
		{
			static assert(!udas.length, errorText);

			static if(is(F == struct))
			{
				xml.onStartTag[xmlName!F] = xml => parse(field, xml);
			}
			else static if(is(F G == G[]))
			{
				static if(is(G == struct))
				{
					xml.onStartTag[xmlName!G] = (xml)
					{ G g; parse(g, xml); field ~= g; };
				}
				else
					static assert(0, errorText);
			}
			else
				static assert(0, errorText);
		}
	}

	xml.parse();
}


template xmlName(T)
{
	alias udas = expressionTuple!(__traits(getAttributes, T));
	static if(udas.length == 1 && is(typeof(udas[0]) == string))
		enum string xmlName = udas[0];
	else
		static assert(0, T.stringof);
}


string getTextOrAttrText(in Element e, in string attrName) @trusted
{ return (attrName.empty ? e.text() : e.getAttrText(attrName)).nonNullString(); };

string tryGetAttrText(in Tag tag, in string attrName) pure nothrow
{
	if(const res = attrName in tag.attr)
		return (*res).nonNullString();
	return null;
}

string getAttrText(in Element e, in string attrName) pure
{
	const res = attrName in e.tag.attr;
	enforce(res);
	return (*res).nonNullString();
}


string nonNullString(string str) pure nothrow
{
	return str ? str : "";
}


template expressionTuple(expressions...) if(isExpressionTuple!expressions)
{
	alias expressionTuple = expressions;
}
