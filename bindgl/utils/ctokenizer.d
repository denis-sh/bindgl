﻿/**
Simple C tokenizer.

Copyright: Denis Shelomovskij 2014

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module bindgl.utils.ctokenizer;


import std.algorithm;
import std.array;
import ascii = std.ascii;
import std.exception;
import uni = std.uni;


@safe pure:

string[] tokenize(string str)
{
	immutable char[2][] twoCharOperators =
	[
		"++", "--",
		"<<", ">>",
		"<=", ">=",
		"==", "!=",
		"&&", "||"
	];

	enum threeCharPunctuator = "...";

	string[] tokens = null;

	while(!str.empty)
	{
		const c = str.front;
		if(uni.isWhite(c))
		{
			str.popFront();
			continue;
		}

		string token = null;

		if(c == '_' || uni.isAlpha(c))
		{
			string after = str.stripLeft!(c => c == '_' || uni.isAlpha(c) || ascii.isDigit(c))();
			token = str[0 .. $ - after.length];
		}
		else if(ascii.isDigit(c) || c == '.' && str.length > 1 && ascii.isDigit(str[1]))
		{
			string after = str.stripLeft!(c => ascii.isAlphaNum(c))();
			if(after.skipOver("."))
				after = after.stripLeft!(c => ascii.isAlphaNum(c))();
			token = str[0 .. $ - after.length];
		}
		else if(const i = str.length >= 2 ? twoCharOperators.countUntil(str[0 .. 2]) + 1 : 0)
		{
			token = twoCharOperators[i - 1];
		}
		else if(str.startsWith(threeCharPunctuator))
		{
			token = threeCharPunctuator;
		}
		else
		{
			enforce(ascii.isASCII(c));
			enforce("#[](){};:.=~!*/%+–<>&|^,".canFind(c));
			token = str[0 .. 1];
		}

		assert(!token.empty);
		tokens ~= token;
		str = str[token.length .. $];
	}

	return tokens;
}

unittest
{
	void test(in string str, in string[] tokens...)
	{
		assert(tokenize(str) == tokens);
	}

	test("a", "a");
	test("_0", "_0");
	test(" a b ", "a", "b");
	test("a=b ", "a", "=", "b");

	test("0 1.2 3. .4 .a", "0", "1.2", "3.", ".4", ".", "a");
	test("0L 1.2L 3.L .4L", "0L", "1.2L", "3.L", ".4L");
	test("1.L.f", "1.L", ".", "f");
	test("1..f ", "1.", ".", "f");

	test("a!=b ", "a", "!=", "b");
	test("a=!b ", "a", "=", "!", "b");
	test("f(t*a...);", "f", "(", "t", "*", "a", "...", ")", ";");
	test("+++a&&&b", "++", "+", "a", "&&", "&", "b");
}

bool equalTokens(in string str1, in string str2)
{
	return str1.tokenize() == str2.tokenize();
}

unittest
{
	assert(equalTokens(" a=b", "a = b "));
	assert(equalTokens("a==b", "a == b"));
	assert(!equalTokens("a!=b", "a! =b"));
}
