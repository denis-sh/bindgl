﻿/**
C type parsing struct.

Copyright: Denis Shelomovskij 2014

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module bindgl.utils.ctypeparser;


import std.algorithm;
import std.exception;
import std.range;
import std.string;
import uni = std.uni;

import bindgl.utils.ctokenizer;


@safe:

struct CType
{
pure:

	enum Kind { basic, struct_, other }

	private
	{
		Kind _kind;
		string _name;
		bool _const;
		bool[] _indirections;
	}

	this(string str)
	{
		this(str.tokenize());
	}

	this(string[] tokens)
	{
		bool currConst = false;
		bool[] indirections = null;
		for(;; tokens.popBack())
		{
			enforce(!tokens.empty);

			const n = tokens[$ - 1].among("*", "const");
			if(!n)
				break;
			if(n == 1)
				indirections = currConst ~ indirections;
			currConst = n == 2;
		}
		_indirections = indirections;
		_const = tokens.skipOver("const") || currConst;

		if(tokens[0] == "struct")
		{
			enforce(tokens.length == 2);
			_kind = Kind.struct_;
			_name = tokens[1];
			return;
		}

		bool signed = false, unsigned = false, short_ = false;
		int long_ = 0;
		string base = null;
		foreach(const token; tokens)
		{
			switch(token)
			{
				case "signed":
					enforce(!unsigned);
					signed = true;
					break;
				case "unsigned":
					enforce(!signed);
					unsigned = true;
					break;
				case "short":
					enforce(!short_ && !long_);
					short_ = true;
					break;
				case "long":
					enforce(!short_);
					enforce(++long_ <= 2);
					break;
				case "int":
					break;
				default:
					enforce(!base);
					base = token;
			}
		}

		if(base)
		{
			enforce(!short_ && long_ != 2);
			if(base == "double")
			{
				_kind = Kind.basic;
				_name = long_ ? "long double" : "double";
			}
			else if(base == "char")
			{
				enforce(!long_);
				_kind = Kind.basic;
				_name = signed ? "signed char" :
					unsigned ? "unsigned char" :
					"char";
			}
			else
			{
				enforce(!signed && !unsigned && !long_);
				_kind = base.among("void", "float") ? Kind.basic : Kind.other;
				_name = base;
			}
		}
		else
		{
			_kind = Kind.basic;
			if(unsigned && !short_ && !long_)
				_name = "unsigned";
			else
				_name = (unsigned ? "unsigned " : "") ~
					(short_ ? "short" :
					long_ == 2 ? "long long" :
					long_ == 1 ? "long" :
					"int");
		}
	}

	@property const nothrow
	{
		Kind kind() { return _kind;}
		string name() { return _name;}
		bool const_() { return _const; }
		const(bool)[] indirections() { return _indirections; }
	}

	string toString() const nothrow
	{
		return (const_ ? "const " : "") ~
			(kind == Kind.struct_ ? "struct " : "") ~
			name ~ indirections.map!`a ? "*const" : "*"`().join();
	}
}

pure unittest
{
	void test(in string type, in string typeString = null, in CType.Kind kind = CType.Kind.basic)
	{
		const cType = const CType(type);
		assert(cType.toString() == (typeString ? typeString : type));
		assert(cType.kind == kind);
	}

	test("void");
	test("char");
	test("signed char");
	test("unsigned char");
	test("int");
	test("long long");
	test("double");
	test("x", "x", CType.Kind.other);
	test("struct x", "struct x", CType.Kind.struct_);

	test("signed", "int");
	test("signed int", "int");
	test("signed short int", "short");
	test("unsigned int", "unsigned");

	test("int signed long", "long");
	test("long int long signed", "long long");
	test("int long int", "long");
	test("int long int long", "long long");

	test("long double");
	test("double long", "long double");

	test(" int *  * ", "int**");
	test("signed long long int *", "long long*");
	test("float *", "float*");
	test("x*", "x*", CType.Kind.other);
	test("struct x*", "struct x*", CType.Kind.struct_);

	test("const int");
	test("const int*");
	test("int const*", "const int*");
	test("const int const*", "const int*");
	test("int*const");
	test("int const*const", "const int*const");
	test("const int*const");
	test("int**const");
	test("int*const*");
}


struct CDecl
{
pure:

	private
	{
		CType _cType;
		string _name;
	}

	this(string[] tokens)
	{
		enforce(tokens.length >= 2);
		_cType = CType(tokens[0 .. $ - 1]);
		_name = tokens[$ - 1];
	}

	@property const nothrow
	{
		const(CType) cType()
		{ return _cType; }

		string name()
		{ return _name; }
	}
}

pure unittest
{
	void test(in string decl, in string typeString, in string name)
	{
		const cDecl = CDecl(decl.tokenize());
		assert(cDecl.cType.toString() == typeString);
		assert(cDecl.name == name);
	}
	test("int a", "int", "a");
	test("signed*a0", "int*", "a0");
}
