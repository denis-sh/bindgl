﻿/**
Module with entry point of OpenGL bindings generator utility.

Copyright: Denis Shelomovskij 2014

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module bindgl.main;


import core.stdc.stdlib: exit, EXIT_FAILURE;

import std.algorithm;
import std.array: empty;
import file = std.file;
import path = std.path;
import io   = std.stdio;
import std.string: format;
import std.traits: packageName;
import std.uni: icmp;
import utf  = std.utf;

import xmldszn = bindgl.utils.xmldeserialization;
import raw     = bindgl.registry.raw;
import bindgl.registry.parse;


enum desc = "OpenGL bindings generator
Copyright: Denis Shelomovskij 2014
Usage:
  BindGL target src-file target-args

  src-file  registry XML file
  target    generation target name

Supported targets:
  check
    target-args: empty
    Just parse registry file without any generation.
";


enum Target { check }

immutable targetNames = [ "check" ];


void main(in string[] args)
{
	if(args.length == 1)
	{
		io.writeln(desc);
		exit(EXIT_FAILURE);
	}

	const target = cast(Target) targetNames.countUntil!`!icmp(a, b)`(args[1]);

	if(target == -1)
		fail("Unknown target '%s'. Supported targets: %-(%s, %).",
			args[1], targetNames);

	if(args.length == 2)
		fail("Incorrect usage: no registry XML file.");

	const srcFile = args[2];

	if(!file.exists(srcFile))
		fail("Registry XML file '%s' doesn't exist.", srcFile);
	
	const xmlRegistry =
	{
		try
			return file.readText(srcFile);
		catch(const file.FileException e)
			fail("I/O error reading registry XML file '%s':\n%s", srcFile, e.msg);
		catch(const utf.UTFException e)
			fail("Registry XML file '%s' isn't a valid UTF-8 text file:\n%s", srcFile, e.msg);
		assert(0);
	}();

	const rawRegistry =
	{
		try
			return xmldszn.deserialize!(raw.Registry)(xmlRegistry);
		catch(const Exception e)
			fail("Error reading registry XML file '%s' raw data:\n%s", srcFile, e.formatMsg());
		assert(0);
	}();

	const registryName = path.baseName!(path.CaseSensitive.no)(srcFile, ".xml");

	const registry =
	{
		try
			return rawRegistry.parse(registryName);
		catch(const Exception e)
			fail("Error parsing registry XML file '%s':\n%s", srcFile, e.formatMsg());
		assert(0);
	}();

	final switch(target)
	{
		case Target.check:
			break;
	}
}


string formatMsg(in Exception e) pure
{
	immutable uninformativeMessages = [ "", "Enforcement failed" ];

	if(uninformativeMessages.canFind(e.msg))
	{
		auto module_ = path.pathSplitter(path.stripExtension(e.file));
		module_.skipOver(packageName!Target);
		return format("%s(%s): %s",
			module_.joiner("."),
			e.line,
			e.msg.empty ? "Thrown an exception" : e.msg);
	}

	return e.msg;
}


void fail(A...)(string fmt, A args)
{
	io.stderr.writefln(fmt, args);
	exit(EXIT_FAILURE);
}
