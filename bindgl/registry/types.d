﻿/**
OpenGL registry types.

Copyright: Denis Shelomovskij 2014

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module bindgl.registry.types;


import std.algorithm;
import std.exception;
import std.math: round;
import std.range;
import std.string;

import bindgl.utils.ctokenizer;
import bindgl.utils.ctypeparser;
static import raw = bindgl.registry.raw;


@safe:

class Registry
{
pure nothrow:

	mixin Property!(string, `name`);
	mixin Property!(Namespace[], `namespaces`);
	mixin Property!(Enums[], `enumGroups`, []);
	mixin Property!(Commands[], `commandGroups`, []);
	mixin Property!(API[], `apis`);

package:

	this(string name)
	{
		_name = name;
	}
}

class API
{
pure nothrow:

	mixin Property!(string, `name`);
	mixin Property!(Type[], `types`);
	mixin Property!(Enum[], `enums`);
	mixin Property!(Version[], `versions`);

package:

	this(string name)
	{
		_name = name;
	}

	void add(Enum enum_)
	{
		_enums ~= enum_;
		enum_._api = this;
	}

	void add(Type type)
	{
		_types ~= type;
		type._api = this;
	}
}

class Type
{
pure:

	enum Kind { struct_, typedef_, apientry, other }

	mixin Property!(string, `name`);
	mixin Property!(Kind, `kind`, cast(Kind) -1);
	/// Required type name, unresolved as it may depend on current API.
	mixin Property!(string, `requiredTypeName`);
	mixin Property!(CType, `cType`, NoInit,
		q{ kind.among(Kind.struct_, Kind.typedef_) });
	mixin Property!(CDecl[], `cParams`, NoInit,
		q{ kind == Kind.apientry });
	mixin Property!(API, `api`);

	const(CType) cReturnType() @property const nothrow
	in { assert(kind == Kind.apientry); }
	body { return _cType; }

package:

	this(const raw.Type raw)
	{
		const textsCount = raw.texts.length;
		enforce((raw.apientry != -1) == (textsCount == 5));
		enforce(!raw.name == (raw.nameText != -1));

		_name = raw.name ? raw.name : raw.texts[raw.nameText];
		_requiredTypeName = raw.requires;
		switch(textsCount)
		{
			case 1:
				enforce(raw.nameText == -1);
				enforce(raw.name.among("stddef", "khrplatform", "inttypes", "GLhandleARB"));

				_kind = Kind.other;
				break;
			case 2:
				enforce(raw.nameText == 0);

				auto tokens0 = raw.texts[0].tokenize();
				enforce(tokens0.length == 2);
				enforce(tokens0[0] == "struct");
				enforce(raw.texts[1].equalTokens(";"));

				_kind = Kind.struct_;
				_cType = CType(_name);
				break;
			case 3:
				enforce(raw.nameText == 1);
				enforce(raw.texts[2].equalTokens(";"));

				auto tokens0 = raw.texts[0].tokenize();
				enforce(tokens0.skipOver("typedef"));

				_kind = Kind.typedef_;
				_cType = CType(tokens0);
				break;
			case 5:
				enforce(raw.nameText == 3);

				auto tokens0 = raw.texts[0].tokenize();
				enforce(tokens0.skipOver("typedef"));
				enforce(tokens0.endsWith("("));
				tokens0.popBack();

				enforce(!raw.texts[1]);
				enforce(raw.texts[2].equalTokens("*"));

				auto tokens4 = raw.texts[4].tokenize();
				enforce(tokens4.skipOver([")", "("]));
				enforce(tokens4.endsWith([")", ";"]));
				tokens4.popBackExactly(2);

				_kind = Kind.apientry;
				_cType = CType(tokens0);
				_cParams = tokens4.splitter(",")
					.map!(toks => CDecl(toks))().array();
				break;
			default:
				enforce(0);
		}
	}
}

class Group
{
pure nothrow:

	mixin Property!(string, `name`);
	mixin Property!(Enum[], `enums`, []);

package:

	this(const raw.Group raw)
	{
		_name = raw.name;
	}

	/// Constructor for creating missed group from enums declaration.
	this(string name)
	{
		_name = name;
	}
}

class Enums
{
pure:

	mixin Property!(Namespace, `namespace`);
	mixin Property!(Group, `group`);
	mixin Property!(bool, `bitmask`);
	mixin Property!(Enum[], `enums`, []);

package:

	this(const raw.Enums raw, Namespace namespace, Group group)
	{
		_namespace = namespace;
		_group = group;
		enforce(!raw.type || raw.type == "bitmask");
		_bitmask = !!raw.type;
	}
}

class Enum
{
pure:

	enum Type { int32, uint32, uint64 }

	mixin Property!(string, `name`);
	mixin Property!(string, `value`);
	mixin Property!(Type, `type`);
	mixin Property!(Group[], `groups`);
	mixin Property!(Enums, `enums`);
	mixin Property!(API, `api`);

package:

	this(const raw.Enum raw, Enums enums, Group[] groups)
	{
		_name = raw.name;
		_value = raw.value;
		if(raw.type)
		{
			const idx = raw.type.among("u", "ull");
			enforce(idx);
			_type = cast(Type) idx;
		}
		_enums = enums;
		_groups = groups;
	}
}

class Commands
{
pure nothrow:

	mixin Property!(Namespace, `namespace`);
	mixin Property!(Command[], `commands`, []);

package:

	this(const raw.Commands raw, Namespace namespace)
	{
		_namespace = namespace;
	}
}

class Command
{
pure:

	mixin Property!(string, `name`);
	mixin Property!(Commands, `commands`);
	mixin Property!(Command, `aliasTo`, null);
	mixin Property!(CType, `cReturnType`);
	mixin Property!(CDecl[], `cParams`);

package:

	this(const raw.Command raw, Commands commands)
	{
		auto tokens = raw.proto.texts.map!tokenize().join();
		enforce((raw.proto.ptype == -1) == tokens.startsWith(["void"], ["void", "*"]));
		_cReturnType = CType(tokens[0 .. $ - 1]);
		_name = tokens[$ - 1];
		_commands = commands;
		_cParams = raw.params
			.map!(param => CDecl(param.texts.map!tokenize().join()))().array();
	}
}

class Namespace
{
pure nothrow:

	mixin Property!(string, `name`);
	mixin Property!(Enums[], `enumGroups`, []);
	mixin Property!(Commands[], `commandGroups`, []);

package:

	this(string name)
	{
		_name = name;
	}
}

class Profile
{
pure nothrow:

	struct Items
	{
		mixin Property!(Type[], `types`);
		mixin Property!(Enum[], `enums`);
		mixin Property!(Command[], `commands`);
	}

	mixin Property!(string, `name`);
	mixin Property!(Items, `added`);
	mixin Property!(Items, `removed`);
	mixin Property!(Items, `restored`);

package:

	this(string name)
	{
		_name = name;
	}
}

class Version
{
	mixin Property!(int, `major`);
	mixin Property!(int, `minor`);
	mixin Property!(string, `preprocessorName`);
	mixin Property!(string, `additionalPreprocessorProtectSymbol`);
	/// Items by profile name.
	mixin Property!(Profile[string], `profiles`);
	mixin Property!(API, `api`);

	int number() @property const pure nothrow
	{ return major * 10 + minor; }

package:

	this(const raw.Feature raw, API api, in Version prev)
	{
		const n = cast(int) round(raw.number * 10);
		if(prev)
			enforce(n > prev.number, "Features are expected to be sorted by version.");

		_major = n / 10;
		_minor = n % 10;
		_preprocessorName = raw.name;
		_additionalPreprocessorProtectSymbol = raw.protect;
		_api = api;

		if(prev)
			enforce(major == prev.major ? minor == prev.minor + 1 : (!prev.major || major == prev.major + 1) && minor == 0,
				format("Unexpected '%s' version number change: %d.%d to %d.%d.",
				api.name, prev.major, prev.minor, major, minor));
		else
			enforce(!minor);

		enforce(preprocessorName.endsWith(format("_%d_%d", major, minor)),
			format("Unexpected '%s' version name: %s.", api.name, preprocessorName));
	}
}

private:

struct NoInit;

mixin template Property(T, string __name, alias init = NoInit, string precondition = null)
{
	static if(is(const T : T)) alias _P = T;
	else alias _P = const T;
	static if(is(init == NoInit))
		enum _init = T.init;
	else
		alias _init = init;

	mixin("package T _" ~ __name ~ " = _init;" ~
		"_P " ~ __name ~ "() @property const pure nothrow" ~
		(precondition ? " in { assert(" ~ precondition ~ "); } body" : "") ~
		"{ return _" ~ __name ~ "; }");
}
