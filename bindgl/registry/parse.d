﻿/**
OpenGL registry definitions processor.

Copyright: Denis Shelomovskij 2014

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module bindgl.registry.parse;


import std.algorithm;
import std.exception;
import std.range;
import std.string;

static import log = bindgl.utils.log;
import bindgl.registry.types;
import bindgl.registry.parseutils;
static import raw = bindgl.registry.raw;


@safe:

Registry parse(const raw.Registry rawRegistry, string registryName) @trusted
{
	alias Group = bindgl.registry.types.Group;

	auto registry = new Registry(registryName);

	registry._namespaces = [new Namespace(null)];
	auto noAPI = new API(null);
	registry._apis = [noAPI];

	auto apiTypes = APIDictionary!Type();
	auto apiEnums = APIDictionary!Enum();
	Namespace[string] namespacesByName = null;
	Command[string] commandsByName;

	// Types
	// ----------------------------------------------------------------------------------------------------

	log.info(0, "Analysing types...");

	foreach(const rawType; rawRegistry.typeGroups.itemsJoiner())
	{
		auto type = apiTypes[rawType.api] = new Type(rawType);
		if(!rawType.api)
			noAPI.add(type);
	}

	{
		// Groups
		// ----------------------------------------------------------------------------------------------------

		log.info(0, "Analysing groups...");

		Group[string] groupsByName = null;
		string[][string] enumNamesByGroupName = null;
		Group[][string] groupsByEnumName = null;

		foreach(const rawGroup; rawRegistry.groupGroups.itemsJoiner())
		{
			enforce(rawGroup.name !in groupsByName, format("Group '%s' redefinition.", rawGroup.name));

			auto group = new Group(rawGroup);
			groupsByName[group.name] = group;
			enumNamesByGroupName[group.name] = rawGroup.enums.dup.sort().release();
			foreach(const enumName; rawGroup.enums)
				groupsByEnumName[enumName] ~= group;
		}

		// Groups from enums
		// ----------------------------------------------------------------------------------------------------

		foreach(const rawEnums; rawRegistry.enumGroups) if(rawEnums.group)
		{
			const name = rawEnums.group;

			auto enumsEnumNames = rawEnums.enumItems.map!`a.name[]`().array().sort().release();

			if(auto groupPtr = name in groupsByName)
			{
				auto group = *groupPtr;

				auto groupEnumNamesPtr = &enumNamesByGroupName[name];
				auto groupEnumNames = *groupEnumNamesPtr;
				auto newEnumNames = setDifference(enumsEnumNames, groupEnumNames).array();
				auto missedEnumNames = setDifference(groupEnumNames, enumsEnumNames);
				if(!newEnumNames.empty || !missedEnumNames.empty)
				{
					log.warning("Mismatch with enums group '%s':", name);
					if(!newEnumNames.empty)
						log.warning("  new: %-(%s, %)", newEnumNames);
					if(!missedEnumNames.empty)
						log.warning("  missed: %-(%s, %).", missedEnumNames);
				}
				if(!newEnumNames.empty)
				{
					*groupEnumNamesPtr = setUnion(groupEnumNames, newEnumNames).array();
					foreach(const enumName; newEnumNames)
						groupsByEnumName[enumName] ~= group;
				}
			}
			else
			{
				log.warning("Unknown enums group '%s'.", name);
				auto group = groupsByName[name] = new Group(name);
				enumNamesByGroupName[name] = enumsEnumNames;
				foreach(const enumName; enumsEnumNames)
					groupsByEnumName[enumName] ~= group;
			}
		}

		// Enums
		// ----------------------------------------------------------------------------------------------------

		log.info(0, "Analysing enums...");

		foreach(const rawEnums; rawRegistry.enumGroups)
		{
			auto namespace = registry._namespaces.getByNameOrAddNew(rawEnums.namespace, namespacesByName);
			auto group = rawEnums.group ? groupsByName[rawEnums.group] : null;

			auto enums = new Enums(rawEnums, namespace, group);
			namespace._enumGroups ~= enums;
			registry._enumGroups ~= enums;

			foreach(const rawEnum; rawEnums.enumItems)
			{
				auto groups = (p => p ? *p : null) (rawEnum.name in groupsByEnumName);
				assert(!group || groups.canFind!`a is b`(group));

				if(!groups.length)
					log.info(2, "Enum '%s' is not in any groups.", rawEnum.name);
				else if(groups.length > 1)
					log.info(1, "Enum '%s' is in multiple groups:\n  groups: %-(%s, %).",
						rawEnum.name, groups.map!`a.name`());

				auto enum_ = apiEnums[rawEnum.api] = new Enum(rawEnum, enums, groups);
				enums._enums ~= enum_;
				if(!rawEnum.api)
					noAPI.add(enum_);
			}
		}
	}

	// Commands
	// ----------------------------------------------------------------------------------------------------

	log.info(0, "Analysing commands...");

	foreach(const rawCommands; rawRegistry.commandGroups)
	{
		auto namespace = registry._namespaces.getByNameOrAddNew(rawCommands.namespace, namespacesByName);

		auto commands = new Commands(rawCommands, namespace);
		registry._commandGroups ~= commands;
		foreach(const rawCommand; rawCommands.items)
		{
			const name = rawCommand.proto.texts[rawCommand.proto.name];
			enforce(name !in commandsByName, format("Command '%s' redefinition.", name));
			commandsByName[name] = new Command(rawCommand, commands);
		}
	}

	foreach(const rawCommand; rawRegistry.commandGroups.itemsJoiner()) if(rawCommand.alias_)
		commandsByName[rawCommand.proto.texts[rawCommand.proto.name]]._aliasTo =
			commandsByName.getByName("aliased command", rawCommand.alias_);

	// Features
	// ----------------------------------------------------------------------------------------------------

	log.info(0, "Analysing features...");

	enforce(!rawRegistry.features.empty);
	auto apiFeatureGroups = rawRegistry.features.map!`a.api[]`().group();
	auto rawFeatures = rawRegistry.features[];
	bool apisByName[string] = null;

	foreach(const apiName, const count; apiFeatureGroups)
	{
		enforce(apiName);
		enforce(apiName !in apisByName, "Features are expected to be grouped by API.");
		apisByName[apiName] = true;

		auto api = new API(apiName);
		registry._apis ~= api;

		foreach(type; apiTypes[apiName])
			api.add(type);

		foreach(type; apiEnums[apiName])
			api.add(type);


		ProfileAvailability[string] profileAvailabilitiesByName = [null : new ProfileAvailability(null)];

		foreach(const i, const rawFeature; rawFeatures.takeExactly(count))
		{
			assert(rawFeature.api == apiName);

			log.info(0, "Analysing feature '%s'...", rawFeature.name);
			auto version_ = new Version(rawFeature, api, i ? api._versions.back : null);
			version_._profiles[null] = new Profile(null);

			foreach(const profileName; chain(rawFeature.requireGroups.map!`a.profile`(), rawFeature.removeGroups.map!`a.profile`()))
			{
				profileName.enforceNullOrNonEmpty();
				if(profileName && profileName !in profileAvailabilitiesByName)
				{
					profileAvailabilitiesByName[profileName] = new ProfileAvailability(profileAvailabilitiesByName[null]);
					version_._profiles[profileName] = new Profile(profileName);
				}
			}

			enforce(rawFeature.removeGroups.all!`a.types.empty`(),
				"Types are expected to not be explicitly removed.");

			// This two enforcements isn't really used in algorithm:
			enforce(rawFeature.removeGroups.all!`!!a.profile`(),
				"Items are expected to be removed only from particular profiles.");

			void update(bool require, string rawGroupKind)(Profile profile)
			{
				auto profileAvailability = profileAvailabilitiesByName[profile.name];
				const rawGroups = __traits(getMember, rawFeature, rawGroupKind);
				// To prevent `Error: template instance ... is nested in both update and Version`:
				auto versionProfiles = version_._profiles;
				alias params = expressionTuple!(require, rawGroups, profile,
					versionProfiles, profileAvailabilitiesByName);

				static if(require)
					updateProfileState!(`type`, name => apiTypes[apiName, name])(params);
				updateProfileState!(`enum`, name => apiEnums[apiName, name])(params);
				updateProfileState!(`command`, name => commandsByName.getByName("command", name))(params);
			}

			/*
			Profiles update consists of the following steps:
				* apply general remove;
				* apply general require;
				* for each profile apply remove, then require.

			General operations apply to all profiles.
			*/
			foreach(j, rawGroupKind; expressionTuple!(`removeGroups`, `requireGroups`))
			{
				alias update_ = update!(j == 1, rawGroupKind);
				update_(version_._profiles[null]);
				foreach(profileName_, profile; version_._profiles)
					if(profileName_)
						update_(profile);
			}

			api._versions ~= version_;
		}
		rawFeatures.popFrontExactly(count);
	}

	return registry;
}
