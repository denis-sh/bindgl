﻿/**
Unprocessed OpenGL registry definitions.

These definitions are base on OpenGL registry Relax NG schema file,
registry.rnc.

Copyright: Denis Shelomovskij 2014

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module bindgl.registry.raw;


alias TypeName = string;
alias Integer = string;

@("registry") struct Registry
{
	@("comment:") string[] comments;
	Types[] typeGroups;
	Groups[] groupGroups;
	Enums[] enumGroups;
	Commands[] commandGroups;
	Feature[] features;
	Extensions[] extensionGroups;
}

@("types") struct Types
{
	Type[] items;
}

@("type") struct Type
{
	string api;
	string requires;
	TypeName name;
	string comment;

	string[] texts;
	@(":") size_t apientry;
	@("name:") size_t nameText;
}

@("groups") struct Groups
{
	Group[] items;
}

@("group") struct Group
{
	/* obligatory */ string name;
	string comment;

	@("enum:name") string[] enums;
}

@("enums") struct Enums
{
	string namespace;
	string group;
	string type; // "bitmask", if present
	Integer start;
	Integer end;
	string vendor;
	string comment;

	Enum[] enumItems;
	Unused[] unusedItems;
}

@("enum") struct Enum
{
	/* obligatory */ Integer value;
	string api;
	string type;
	/* obligatory */ string name; // C numeric type suffix, e.g. 'u' or 'ull'
	@("alias") string alias_;
	string comment;
}

@("unused") struct Unused
{
	Integer start;
	Integer end;
	string comment;
}

@("commands") struct Commands
{
	string namespace;
	Command[] items;
}

@("command") struct Command
{
	@("proto") struct Proto
	{
		string group;
		string[] texts;
		@(":") size_t ptype;
		@(":") /* obligatory */ size_t name;
	}

	@("param") struct Param
	{
		string group;
		string len;
		string[] texts;
		@(":") size_t ptype;
		@(":") /* obligatory */ size_t name;
	}

	@("glx") struct GLX
	{
		/* obligatory */ string text;
		/* obligatory */ int opcode;
		string name;
		string comment;
	}


	string comment;

	/* obligatory */ Proto proto;
	Param[] params;
	@("alias:name") string alias_;
	@(":name") string vecequiv;
	GLX[] glxs;
}

@("feature") struct Feature
{
	/* obligatory */ string api;
	/* obligatory */ string name;
	/* obligatory */ float number;
	string protect;
	string comment;

	Require!false[] requireGroups;
	Remove!false[] removeGroups;
}

@("extensions") struct Extensions
{
	Extensions[] items;
}

@("extension") struct Extension
{
	/* obligatory */ string name;
	string protect;
	string supported; // regular expression
	string comment;

	Require!true[] requireGroups;
	Remove!true[] removeGroups;
}


@("require") struct Require(bool withAPI)
{
	static if(withAPI) string api;
	string profile;
	string comment;

	InterfaceType[] types;
	InterfaceEnum[] enums;
	InterfaceCommand[] commands;
}

@("remove") struct Remove(bool withAPI)
{
	static if(withAPI) string api;
	string profile;
	string comment;

	InterfaceType[] types;
	InterfaceEnum[] enums;
	InterfaceCommand[] commands;
}

@("type") struct InterfaceType
{
	/* obligatory */ string name;
	string comment;
}

@("enum") struct InterfaceEnum
{
	/* obligatory */ string name;
	string comment;
}

@("command") struct InterfaceCommand
{
	/* obligatory */ string name;
	string comment;
}
