﻿/**
Utilities for OpenGL registry definitions processor.

Copyright: Denis Shelomovskij 2014

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module bindgl.registry.parseutils;


import std.algorithm;
import std.exception;
import std.range;
import std.string;
import std.traits: isExpressionTuple;

static import log = bindgl.utils.log;
import bindgl.registry.types;


@safe:

struct APIDictionary(T)
{
pure:

	string[] apis;
	T[string][string] _data;

	ref T opIndexAssign(T val, string api)
	{
		api.enforceNullOrNonEmpty();

		if(auto p = api in _data)
		{
			enforce(val.name !in *p, format("'%s' already exists in '%s'.", val.name, api));
			return (*p)[val.name] = val;
		}
		else
		{
			apis ~= api;
			return (_data[api] = [val.name : val])[val.name];
		}
	}

	// Objects with specialized API takes priority over general ones.
	ref T opIndex(string api, string name)
	{
		api.enforceNullOrNonEmpty();

		if(auto p = api in _data)
		{
			if(T* res = name in *p)
				return *res;
			enforce(null in _data);
		}

		enforce(name in _data[null]);
		return _data[null][name];
	}

	T[string] opIndex(string api)
	{
		api.enforceNullOrNonEmpty();
		if(auto res = api in _data)
			return *res;
		return null;
	}
}

void enforceNullOrNonEmpty(string str) pure
{
	enforce(!str || !str.empty);
}

T getByName(T)(T[string] byName, string kind, string name) pure
{
	T* res = name in byName;
	enforce(res, format("Unknown %s '%s'.", kind, name));
	return *res;
}

ref T getByNameOrAddNew(T)(ref T[] ordered, string name, T[string] byName) pure nothrow
{
	if(T* res = name in byName)
		return *res;
	ordered ~= new T(name);
	return byName[name] = ordered[$ - 1];
}

enum State { available, removed }

class ProfileAvailability
{
pure:

	bool[string] types = null, enums = null, commands = null;

	this(ProfileAvailability general)
	{
		if(general)
		{
			types = general.types.dup();
			enums = general.enums.dup();
			commands = general.commands.dup();
		}
	}
}

void updateProfileState(string kind, alias getByName = null, T)(in bool require,
	in T rawGroups, Profile profile,
	Profile[string] profiles,
	ProfileAvailability[string] availabilities) @trusted
{
	enum field = kind ~ 's';

	bool update(in string name, Profile currProfile)
	{
		string getMsgPostfix()
		{
			return currProfile.name ? format(" by '%s' profile", currProfile.name) : "";
		}

		auto availability = availabilities[currProfile.name];

		bool* availabilePtr = name in __traits(getMember, availability, field);
		const availabile = availabilePtr && *availabilePtr;

		if(require)
		{
			if(availabile)
			{
				if(currProfile is profile)
					log.warning("Required already available %s '%s'%s.",
						kind, name, getMsgPostfix());
				return false;
			}

			if(availabilePtr && currProfile is profile)
				log.info(0, "Restored %s '%s'%s.",
					kind, name, getMsgPostfix());

			__traits(getMember, availability, field)[name] = true;
			__traits(getMember, !availabilePtr ? currProfile._added : currProfile._restored, '_' ~ field) ~= getByName(name);
		}
		else
		{
			if(!availabile)
			{
				if(currProfile is profile)
					log.warning("Removed %s %s '%s'%s.",
						availabilePtr ? "already unavailable" : "not added",
						kind, name, getMsgPostfix());
				return false;
			}

			*availabilePtr = false;
			/* dmd @@@BUG@@@ workaround
			__traits(getMember, currProfile._removed, '_' ~ field) ~= getByName(name);
			*/
			mixin("currProfile._removed._" ~ field) ~= getByName(name);
		}
		return true;
	}

	foreach(const rawGroup; rawGroups) if(rawGroup.profile == profile.name)
	{
		foreach(const raw; __traits(getMember, rawGroup, field))
		{
			if(!update(raw.name, profile) || profile.name)
				continue;

			foreach(otherProfile; profiles.byValue())
				if(otherProfile.name)
					update(raw.name, otherProfile);
		}
	}
}


auto itemsJoiner(T)(T[] array) pure nothrow
{
	return array.map!`a.items[]`().joiner();
}

T[] getByNames(T, R)(T[string] byName, string kind, R names, ref bool[string] usedNames)
{
	T[] res;
	foreach(const name; names)
	{
		if(name in usedNames)
		{
			log.warning("Duplicate usage %s '%s'.", kind, name);
			continue;
		}
		usedNames[name] = true;
		res ~= byName.getByName(kind, name);
	}
	return res;
}

template expressionTuple(expressions...) if(isExpressionTuple!expressions)
{
	alias expressionTuple = expressions;
}
